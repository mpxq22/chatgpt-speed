# chatgpt-speed

#### 介绍
Open AI ChatGPT，php实现chatgpt流式输出;连续对话，支持GPT3.5/GPT4，快速体验搭建你的问答机器人(不会的私信提供技术指导)

  **本项目非常简单易懂仅做参考使用，实现了流式输出 连续对话。大家使用过程中有什么bug或者对程序有什么好的提议都可以向我咨询。** 
 
  **当然大家在开发过程中有任何的问题也可以向我咨询。您的 :star: star是我们前进的动力** 

| chatgpt交流群 | 群过期私信拉群 |
|------------|---------|
|  ![输入图片说明](public/static/group.jpg)          |     ![输入图片说明](public/static/1.jpg)    |


#### 软件架构
1.后端技术栈 php thinkphp6

2.前端技术栈 html js jq

#### 项目部署
 1.拉取项目源码 <br>
https://gitee.com/haoyachengge/chatgpt-speed.git<br><br>
 2.env配置<br>
修改根目录.env文件，根据字段说明配置数据库信息以及调试模式等<br><br>
3.接口配置<br>
修改/app/ai/config/open.php，根据字段说明配置接口域名以及apiKey等。<br>
 关于接口域名的配置<br>1.如果主程序放海外服务器的，直接用官方的接口域名（https://api.openai.com）<br>2.用海外服务器做反向代理，配置反向代理（推荐） <br><br>
4.sql<br>
根目录db.sql，导入数据库<br><br>
5.网站运行目录为public,项目部署成功后直接输入域名即可 
#### 功能截图
![输入图片说明](public/static/1685788760170.jpg)
#### 疑难解答
有小伙伴说流式模式会有漏字问题，这是由于网络传输的问题，返回的数据不一定完整，该程序已经解决，小伙伴们可以参考一下解决办法

 
        

